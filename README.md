# gender_classification

***

## Description
Gender classification via handcrafted features such as pitch formants extracted using the praat software from the librispeech corpus.